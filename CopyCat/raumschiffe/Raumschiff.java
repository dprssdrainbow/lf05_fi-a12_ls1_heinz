package raumschiffe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**@author Sophia Heinz
 * @version 3 */
public class Raumschiff {

    private Ladung ladung;
    private ArrayList<Ladung> ladungen = new ArrayList<Ladung>();
    private int photonenTorpedoAnzahl = 0;
    private int energieversorgungInProzent = 100;
    private int schildeInProzent = 100;
    private int huelleInProzent = 100;
    private int lebenserhaltungssystemeInProzent = 100;
    private int androidenAnzahl = 0;
    private String schiffsname;
    private String logBuch[] = new String[100];

    /**Constructor*/
    public Raumschiff(String schiffsname) {
        this.schiffsname = schiffsname;
        this.androidenAnzahl = 0;
        this.photonenTorpedoAnzahl = 0;
    }

    /**Constructor*/
    public Raumschiff (String schiffsname, int androidenAnzahl) {
        this.schiffsname = schiffsname;
        this.androidenAnzahl = androidenAnzahl;
        this.photonenTorpedoAnzahl = 0;
    }

    /**Constructor*/
    public Raumschiff (String schiffsname, int androidenAnzahl, int photonenTorpedoAnzahl) {
        this.schiffsname = schiffsname;
        this.androidenAnzahl = androidenAnzahl;
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
    }

    /**Funktion um dem Schiff einem Namen zu geben*/
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    /**Funktion um den Schiffsnamen auszulesen*/
    public String getSchiffsname() {
        return this.schiffsname;
    }

    /**Funktion um die Schilde zu setzen*/
    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    /**Funktion um den Status der Schilde auszulesen*/
    public int getSchildeInProzent() {
        return this.schildeInProzent;
    }

    /**Funktion um die Anzahl der Topedos zu setzen*/
    public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
    }

    /**Funktion um die Anzahl der Torpedos auszulesen*/
    public int getPhotonenTorpedoAnzahl() {
        return this.photonenTorpedoAnzahl;
    }

    /**Funktion um die Energieversorgung zu setzen*/
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    /**Funktion um den Status der Energieversorgung auszulesen*/
    public int getEnergieversorgungInProzent() {
        return this.energieversorgungInProzent;
    }

    /**Funktion um die Hülle zu setzen*/
    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    /**Funktion um den Status der Hülle auszulesen*/
    public int getHuelleInProzent() {
        return this.huelleInProzent;
    }

    /**Funktion um die Lebenserhaltungssysteme zu setzen*/
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    /**Funktion um den Status der Lebenserhaltungssysteme auszulesen*/
    public int getLebenserhaltungssystemeInProzent() {
        return this.lebenserhaltungssystemeInProzent;
    }

    /**Funktion um die Anzahl der Androiden an Board zu setzen*/
    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    /**Funktion um die Anzahl der Androiden auszulesen*/
    public int getAndroidenAnzahl() {
        return this.androidenAnzahl;
    }

    /**Funktion um eine Ladung zum Raumschiff hinzuzufügen*/
    public void addLadung(Ladung ladung) {
        this.ladungen.add(ladung);
    }

    /**Funktion die den Zustand des Raumschiffs in der Konsole ausgibt*/
    public void zustandDesRaumschiffsAusgeben() {
        System.out.printf("\n\nZustand vom Raumschiff [%s]\n " +
                        "Photonentorpedos: %d \n " +
                        "Energieversorgung: %d%% \n " +
                        "Schilde: %d%% \n " +
                        "Hülle: %d%% \n " +
                        "Lebenserhaltungssysteme: %d%% \n " +
                        "Androiden: %d\n",
                this.schiffsname, this.photonenTorpedoAnzahl, this.energieversorgungInProzent, this.schildeInProzent, this.huelleInProzent, this.lebenserhaltungssystemeInProzent, this.androidenAnzahl);
        logBuchEintrag("Zustand vom Raumschiff überprüft");
    }

    /**Funktion die das Ladungsverzeichnis in der Konsole ausgibt*/
    public void ladungsverzeichnisAusgeben() {
        System.out.printf("\n\nFolgende Objekte sind im Raumschiff [%s] geladen:\n", this.schiffsname);
        for (Ladung element : ladungen) {
            if (element.getMenge() <= 0) {
            } else {
                System.out.print(" " + element.getBezeichnung() + " (" + element.getMenge() + ") // [" + element + "]");
                System.out.printf("\n");
            }
        }
        logBuchEintrag("Ladungsverzeichnis überprüft");
    }

    /**Funktion die einen Torpedo verschießt und die Anzahl dementsprechend verringerrt
     * @param anzahl Die Anzahl an geschossenen Torpedos*/
    public void photonenTorpedosAbschießen(int anzahl) {
        if (photonenTorpedoAnzahl < anzahl) {
            photonenTorpedoAnzahl -= this.photonenTorpedoAnzahl;
            System.out.printf("\n\nPhotonentorpedo abgeschossen!");
            System.out.printf("\n\nKonsole: (%d) Photonentorpedo(s) eingesetzt", this.photonenTorpedoAnzahl);
            torpedoVerladen(this.photonenTorpedoAnzahl);
        } else if (this.photonenTorpedoAnzahl == 0) {
            System.out.printf("\nKonsole: Keine Photonentorpedos gefunden!");
            System.out.printf("\n\n-=*Click*=-");
            torpedoVerladen(anzahl);
        } else {
            photonenTorpedoAnzahl -= anzahl;
            System.out.printf("\n\nPhotonentorpedo abgeschossen!");
            System.out.printf("\n\nKonsole: (%d) Photonentorpedo(s) eingesetzt", anzahl);
            torpedoVerladen(anzahl);
        }
        logBuchEintrag("Photonentorpedo wurde abgeschossen");
    }

    /**Funktion um torpedos aus dem Ladungsverzeichnis zu verladen*/
    public void torpedoVerladen(int anzahl) {
        for (Ladung element : ladungen) {
            if (element.getBezeichnung() == "Photonentorpedo") {
                if (element.getMenge() < anzahl) {
                    this.photonenTorpedoAnzahl += element.getMenge();
                    element.setMenge(0);
                    ladungen.remove(element);
                } else {
                    this.photonenTorpedoAnzahl += anzahl;
                    element.setMenge(element.getMenge() - anzahl);
                }
            }
        }
    }

    /**Funktion um die Phaserkanone abzuschießen die dabei die Energieversorung um die Hälfte verringert*/
    public void phaserKanoneAbschießen() {
        if (energieversorgungInProzent >= 50) {
            energieversorgungInProzent -= 50;
            System.out.printf("\n\nPhaserkanone abgeschossen!");
        } else {
            System.out.printf("\n\n-=*Click*=-");
        }
        logBuchEintrag("Phaserkanone wurde abgeschossen");
    }

    /**Funktion die einen oder meherere Treffer vermerkt und den Zustand vom Raumschiff anpasst
     * @param anzahl Die Anzahl wie oft das Raumschiff getroffen wurde*/
    public void trefferVermerken(int anzahl) {
        System.out.printf("\n\n[%s] wurde getroffen!", this.schiffsname);
        if (this.schildeInProzent > 50*anzahl) {
            this.schildeInProzent -= 50*anzahl;
        } else if (this.schildeInProzent <= 50*anzahl) {
            this.schildeInProzent -= this.schildeInProzent;

            this.huelleInProzent -= 50*anzahl;
            if (this.energieversorgungInProzent > 50*anzahl) {
                this.energieversorgungInProzent -= 50*anzahl;
            } else {
                this.energieversorgungInProzent -= this.energieversorgungInProzent;
            }
        } else {
            if (this.huelleInProzent <= 50*anzahl) {
                this.huelleInProzent -= this.huelleInProzent;
                this.lebenserhaltungssystemeInProzent -= this.lebenserhaltungssystemeInProzent;
                System.out.printf("Lebenserhaltungssystem vom Raumschiff [%s] wurden zerstört!", this.schiffsname);
            }
        }
        logBuchEintrag("Von Gegner getroffen");
    }

    /**Funktion um die Androiden an Board einzusetzen um somit das Raumschiff zu reparieren/verbessern*/
    public void reparaturAndroidenEinsetzen(int anzahl, boolean energieversorgung, boolean huelle, boolean schilde) {
        Random rand = new Random(); int n = rand.nextInt(100); n += 1;
        int strukturen = 0; if (energieversorgung) {strukturen++;} if (huelle) {strukturen++;} if (schilde) {strukturen++;}
        if (anzahl > this.androidenAnzahl) {
            float ergebnis = n*this.androidenAnzahl/strukturen;
            if (energieversorgung) {this.energieversorgungInProzent += ergebnis;} if (huelle) {this.huelleInProzent += ergebnis;} if (schilde) {this.schildeInProzent += ergebnis;}
        } else {
            float ergebnis = n*anzahl/strukturen;
            if (energieversorgung) {this.energieversorgungInProzent += ergebnis;} if (huelle) {this.huelleInProzent += ergebnis;} if (schilde) {this.schildeInProzent += ergebnis;}
        }
    }

    /**Funktion die eine Nachricht in der Konsole ausgibt
     * @param nachricht Die Nachricht die dann in der Konsole ausgeben und im Logbuch vom Raumschiff gespeichert wird*/
    public void nachrichtenAnAlle(String nachricht) {
        System.out.printf("\n\n%s: %s\n", this.schiffsname, nachricht);
        logBuchEintrag("Folgende Nachricht wurde versendet: " + nachricht);
    }

    /**Funktion die alle aufgerufenen Funktionen eines Raumschiffs ausgibt*/
    public void logBuchEintraegezZurueckgeben() {
        System.out.printf("\nLogbuch Einträge");
        for (int c = 1; c < this.logBuch.length && this.logBuch[c] != null; c++) {
            System.out.printf("\n");
            System.out.print(this.logBuch[c]);
        }
    }

    /**Funktion die alle aufgerufenen Funktionen mit Mehrwert protokolliert und im entsprechenden Raumschiff speichert
     * @param input Die Funktion die aufgerufen wurde*/
    private void logBuchEintrag(String input) {
        Date getDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(" [dd-MM-yyyy | HH:mm:ss] ");
        String date = formatter.format(getDate);
        int c = 1;
        while (c < this.logBuch.length-1) {
            if (this.logBuch[c] == null) {
                this.logBuch[c] = date + input;
                break;
            } else {
                c++;
            }
        }
    }
}
