package raumschiffe;

public class Ladung {
    String bezeichnung;
    int menge;

    public Ladung() {}
    public Ladung(String bezeichnung) {this.bezeichnung = bezeichnung;}
    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public int getMenge() {
        return this.menge;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBezeichnung() {
        return this.bezeichnung;
    }
}