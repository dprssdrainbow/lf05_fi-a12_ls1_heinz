package raumschiffe;

import java.util.ArrayList;

public class Controller {

    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta");
        klingonen.setAndroidenAnzahl(2); klingonen.setPhotonenTorpedoAnzahl(3);
        Ladung l1_1 = new Ladung("Ferengi Schneckensaft", 200); klingonen.addLadung(l1_1);
        Ladung l1_2 = new Ladung("Bet'leth Klingonen Schwert", 200); klingonen.addLadung(l1_2);

        Raumschiff romulaner = new Raumschiff("IRW Khazara", 2);
        romulaner.setPhotonenTorpedoAnzahl(2); romulaner.setSchildeInProzent(200);
        Ladung l2_1 = new Ladung("Borg-Schrott", 5); romulaner.addLadung(l2_1);
        Ladung l2_2 = new Ladung("Rote Materie", 2); romulaner.addLadung(l2_2);
        Ladung l2_3 = new Ladung("Plasma Waffe", 50); romulaner.addLadung(l2_3);

        Raumschiff vulkanier = new Raumschiff("N'Var", 5, 0);
        Ladung l3_1 = new Ladung("Forschungssonde", 35); vulkanier.addLadung(l3_1);

        klingonen.photonenTorpedosAbschießen(1); romulaner.trefferVermerken(1);

        romulaner.phaserKanoneAbschießen(); klingonen.trefferVermerken(1);

        vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");

        klingonen.zustandDesRaumschiffsAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reparaturAndroidenEinsetzen(5, true, true, true);
        Ladung l3_2 = new Ladung("Photonentorpedo", 3);
        vulkanier.addLadung(l3_2); vulkanier.torpedoVerladen(3); //l3_2.ladungAufräumen();
        klingonen.photonenTorpedosAbschießen(2); romulaner.trefferVermerken(2);

        klingonen.zustandDesRaumschiffsAusgeben();
        klingonen.logBuchEintraegezZurueckgeben();
        klingonen.ladungsverzeichnisAusgeben();

        romulaner.zustandDesRaumschiffsAusgeben();
        romulaner.logBuchEintraegezZurueckgeben();
        romulaner.ladungsverzeichnisAusgeben();

        vulkanier.zustandDesRaumschiffsAusgeben();
        vulkanier.logBuchEintraegezZurueckgeben();
        vulkanier.ladungsverzeichnisAusgeben();
    }
}
