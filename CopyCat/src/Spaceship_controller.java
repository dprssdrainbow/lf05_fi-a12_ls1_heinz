package src;

import java.util.Arrays;
import java.util.Scanner;

public class Spaceship_controller {
	static Object Player[] = new Object[16];
	static String[] chatHistory = new String[1000];
	static Scanner console = new Scanner(System.in);
	static int selectedPlayer = 404;
	static String Playerlist = "";

	public static void main(String[] args) {		
		Player[0] = new Spaceship("Admin");
		Player[1] = new Spaceship("Jesse");
		Player[2] = new Spaceship("Sophia");	
		Player[3] = new Spaceship("Artur");
		Player[4] = new Spaceship("Hugo");	
		Player[5] = new Spaceship("Faradjollah");	
		Player[6] = new Spaceship("Hafezi");	
		Player[7] = new Spaceship("Lena");
		Player[8] = new Spaceship("Sanam");
		showPlayerList();
		selectPlayer();	
	}
	
	public static void shootPlayer() {
		System.out.println("Which ship do u want to shoot ?");
		System.out.printf("\nSpaceship's in this System: " + Playerlist + "\n");
		System.out.printf("\nInput[id]: ");
		int enemy = console.nextInt();
		if (((Spaceship) Player[selectedPlayer]).current_torpedo == "none") {
			System.out.printf("\nNo Torpedo loaded!\n\n");
		} else {
			((Spaceship) Player[enemy]).getDamage(((Spaceship) Player[selectedPlayer]).current_torpedo);
			((Spaceship) Player[selectedPlayer]).getTokens(((Spaceship) Player[selectedPlayer]).current_torpedo);
			((Spaceship) Player[selectedPlayer]).current_torpedo = "none";
			if (((Spaceship) Player[enemy]).health < 1) {
				Player[enemy] = "destroyed";
				showPlayerList();
				((Spaceship) Player[enemy]).selfDestruction();
			} else {
				((Spaceship) Player[selectedPlayer]).sendStatus();
			}
		}
	}
	
	public static void showPlayerList() {
		Playerlist = "";
		int counter = 1;	
		while (Player[counter]!= null) {
			if (Player[counter] == "destroyed") {
				counter++;
			} else {
				Playerlist = Playerlist + ((Spaceship) Player[counter]).getName() + "[id: " + counter +"], ";
				counter++;
			}
		}
		System.out.println("Spaceship's in this System: " + Playerlist);
		if (selectedPlayer == 404) {
			selectPlayer();
		} else {
			((Spaceship) Player[selectedPlayer]).sendStatus();
		}
	}
	
	public static void selectPlayer() {
		System.out.printf("\nWhich Ship do u want to control ?\nInput [id]: ");
		selectedPlayer = console.nextInt();
		((Spaceship) Player[selectedPlayer]).sendStatus();
	}
	
	public static void destroySpaceship() {
		Player[selectedPlayer] = "destroyed";
		System.out.println(Player[selectedPlayer]);
		selectedPlayer = 404;
		showPlayerList();
		((Spaceship) Player[selectedPlayer]).selfDestruction();
	}
	
	public static void destroyUniverse() {
		System.exit(0);
	}

}
