
public class Singleton {

	private static Singleton instance;
	private String username;
	private String password;

	private Singleton(String usr, String pwd) {
		this.username = usr;
		this.password = pwd;
	}
	
	public static Object Instance(String usr, String pwd) {
		if (instance == null) {
			instance = new Singleton(usr, pwd);
		}
		return instance;
	}
	
	public String getName() {
		return this.username;
	}
	

}
