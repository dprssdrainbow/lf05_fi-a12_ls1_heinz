package src;
import java.util.Scanner;

public class AutoTest {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        Auto a1 = new Auto("Ford", "Blau");

        System.out.printf("Das Auto von der Marke %s hat die Farbe %s!", a1.getMarke(), a1.getFarbe());
    }
}
