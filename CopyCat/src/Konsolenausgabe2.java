package src;

import java.util.Scanner;
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		System.out.printf("Willkommen zu meinem Programm !\n- Konsolenausgabe -\nEntiwckelt von Sophia Heinz/oszimt/FI-A12");
		AufgabenAuswahl();	
	
	}
	
	public static void AufgabenAuswahl() {
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("\n\nWählen Sie eine Aufgabe: 1 / 2 / 3\n0 um das Programm zu beenden.\nEingabe: ");
		int i = tastatur.nextInt();
		
		switch (i) {
		case 0:
			System.out.println("\nDanke fürs Verwenden meines Programms !");
			return;
		case 1:
			Aufgabe1();
			break;
		case 2:
			Aufgabe2();
			break;
		case 3:
			Aufgabe3();
			break;
		case 4:
			Aufgabe0();
			break;
		default:
				System.out.println("Falsche Eingabe !\n");
				AufgabenAuswahl();
				break;
		}
	}

	public static void Aufgabe1() {
		
		String str = "**";
		System.out.printf("%4.2s\n%.1s%5.1s\n%.1s%5.1s\n%4.2s", str, str, str, str, str, str);
		AufgabenAuswahl();
	}
	
	public static void Aufgabe2() {
		
		String str = "**";
		System.out.printf("0!%3.0s=%19.0s=%2.0s 0\n1!%3.0s= 1 * %14.0s=%2.0s 1\n2!%3.0s= 1 * 2%13.0s=%2.0s 1\n3!%3.0s= 1 * 2 * 3%9.0s=%2.0s 6\n4!%3.0s= 1 * 2 * 3 * 4%5.0s=%1.0s 24\n5!%3.0s= 1 * 2 * 3 * 4 * 5 = 120", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
		AufgabenAuswahl();
	}
	
	public static void Aufgabe3() {
		
		int[] fahrenheit = {-20, -10, 0, 20, 30};
		double[] celsius = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
		int counter = 0;
		
		String str_0 = "------------------------";
		String str_1 = "Fahrenheit";
		String str_2 = "Celsius";
		
		System.out.printf("%-12s|%10s\n%s", str_1, str_2, str_0);
		
		while (counter < 2) {
			System.out.printf("\n%-12d|%10.2f", fahrenheit[counter], celsius[counter]);
			counter++;
		}
		
		while (counter < 5) {
			System.out.printf("\n+%-11d|%10.2f", fahrenheit[counter], celsius[counter]);
			counter++;
		}
		AufgabenAuswahl();
	}
	public static void Aufgabe0() {
		int c = 1;
		for (int i = 0; i < 10; i = i +1, c++) {
			System.out.printf("%d : %d\n", i, c);
		}

		int j =  14;
		j = j%3;

		System.out.print(j);

		for (int i = j; i <5; i++) {
			System.out.print(i);
		}
	}
}
