import java.util.Scanner;

public class Konsolenausgabe {

	public static void main(String[] args) {
		
		System.out.printf("Willkommen zu meinem Programm !\n- Konsolenausgabe -\nEntiwckelt von Sophia Heinz/oszimt/FI-A12");
		AufgabenAuswahl();
		
	}
	
	public static void AufgabenAuswahl() {
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("\n\nWählen Sie eine Aufgabe: 1 / 2 / 3 / 4\n0 um das Programm zu beenden.\nEingabe: ");
		int i = tastatur.nextInt();
		
		switch (i) {
		case 0:
			System.out.println("\nDanke fürs Verwenden meines Programms !");
			return;
		case 1:
			Aufgabe1();
			break;
		case 2:
			Aufgabe2();
			break;
		case 3:
			Aufgabe3();
			break;
		case 4: 
			Aufgabe3neu();
			break;
		default:
				System.out.println("Falsche Eingabe !\n");
				AufgabenAuswahl();
				break;
		}
	}
	
	public static void Aufgabe1() {
		
	
		
		String str_1 = "Das ist ein Beispielsatz.";
		String str_2 = "Das ist der 2. Beispielsatz.";
		System.out.print(str_1 + "\n" + str_2);
		System.out.printf("\n%s\n%s", str_1, str_2);
		
		//.print schreibt etwas in die Konsole ohne einen Zeilenumbruch
		//.println schreibt etwas in die Konsole mit einem Zeilenumbruch
		//.printf schreibt formatierten text in die Konsole
		AufgabenAuswahl();
	}
	
	public static void Aufgabe2() {
		
		String str = "*************";
		System.out.printf("%13.1s\n%14.3s\n%15.5s\n%16.7s\n%17.9s\n%18.11s\n%19.13s\n%14.3s\n%14.3s", str, str, str, str, str, str, str, str, str);
		AufgabenAuswahl();
	}
	
	public static void Aufgabe3() {
		
		double nb_1 = 22.4234234;
		double nb_2 = 111.2222;
		double nb_3 = 4.0;
		double nb_4 = 1000000.551;
		double nb_5 = 97.34;		
		System.out.printf("%.2f \n%.2f \n%.2f \n%.2f \n%.2f", nb_1, nb_2, nb_3, nb_4, nb_5);
		AufgabenAuswahl();
	}
	
	public static void Aufgabe3neu() {
		
		double[] arr = {22.4234234, 111.2222, 4.0, 1000000.551, 97.34};
		int counter = 0;
		
		while (counter < arr.length) {
			System.out.printf("%.2f\n", arr[counter]);
			counter++;
		}
		AufgabenAuswahl();
	}
}
