import java.util.Scanner;
public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		
		System.out.println("was möchten Sie bestellen?");
		String artikel = liesString();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();

		
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		

	}

	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		String hugo = myScanner.next();
		return hugo;
	}
	
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		int hugo = myScanner.nextInt();
		return hugo;
	}
	
	public static double liesDouble() {
		Scanner myScanner = new Scanner(System.in);
		double hugo = myScanner.nextDouble();
		return hugo;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
