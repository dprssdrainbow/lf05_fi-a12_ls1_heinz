package src;

import java.util.Scanner;

class Fahrkartenautomat
{
    static double zuZahlenderBetrag;
    static double AnzahlTickets;
    static double eingezahlterGesamtbetrag;
    static double eingeworfeneMünze;
    static double rückgabebetrag;
    static double anzahlKaufenderTickets;
	static String originalPreis;
    static String[][] Fahrscheine = new String[12][2];
	static Scanner tastatur = new Scanner(System.in);
    
    public static void main(String[] args)
    {

		Fahrscheine[1][0] = "Einzelfahrschein Berlin AB";
		Fahrscheine[1][1] = "2.90";

		Fahrscheine[2][0] = "Einzelfahrschein Berlin BC";
		Fahrscheine[2][1] = "3.30";

		Fahrscheine[3][0] = "Einzelfahrschein Berlin ABC";
		Fahrscheine[3][1] = "2.90";

		Fahrscheine[4][0] = "Kurzstrecke";
		Fahrscheine[4][1] = "1.90";

		Fahrscheine[5][0] = "Tageskarte Berlin AB";
		Fahrscheine[5][1] = "8.60";

		Fahrscheine[6][0] = "Tageskarte Berlin BC";
		Fahrscheine[6][1] = "9.00";

		Fahrscheine[7][0] = "Tageskarte Berlin ABC";
		Fahrscheine[7][1] = "9.60";

		Fahrscheine[8][0] = "Kleingruppen-Tageskarte Berlin AB";
		Fahrscheine[8][1] = "23.50";

		Fahrscheine[9][0] = "Kleingruppen-Tageskarte Berlin BC";
		Fahrscheine[9][1] = "24.30";

		Fahrscheine[10][0] = "Kleingruppen-Tageskarte Berlin ABC";
		Fahrscheine[10][1] = "24.90";
        System.out.print("Anzahl der Tickets: ");
        AnzahlTickets = tastatur.nextDouble();
		fahrkartenbestellungErfassen();
		fahrkartenBezahlen();
    }
    
    public static void fahrkartenbestellungErfassen() {
		int counter = 1;
		System.out.println("Verfügbare Fahrscheine:");
		while (Fahrscheine[counter][0] != null) {
			System.out.printf("%s [%s€] (%d)\n", Fahrscheine[counter][0], Fahrscheine[counter][1], counter);
			counter++;
		}
		if (AnzahlTickets > 0) {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("Welches Ticket möchten Sie kaufen ?");
			int ticketAuswahl = tastatur.nextInt();
			if (ticketAuswahl >= Fahrscheine.length-1){System.out.println("Ungültige Eingabe!");fahrkartenbestellungErfassen();}
			originalPreis = Fahrscheine[ticketAuswahl][1];
			System.out.println("Wie viele Tickets möchten Sie kaufen?");
			anzahlKaufenderTickets = tastatur.nextDouble();
			if (anzahlKaufenderTickets > AnzahlTickets) {
				System.out.printf("Es gibt nur noch %.0f Ticket/s im Automaten\n", AnzahlTickets);
				fahrkartenbestellungErfassen();
			} else {
				fahrkartenBezahlen();
			}
		} else {
			System.out.println("\nKeine Fahrscheine mehr!");
			System.exit(0);
		}
    }
    
    public static void fahrkartenBezahlen() {
		eingezahlterGesamtbetrag = 0.0;
		zuZahlenderBetrag = Double.parseDouble(originalPreis);
		zuZahlenderBetrag = zuZahlenderBetrag*anzahlKaufenderTickets;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.printf("Noch zu zahlen: %.2f€\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2€): ");
			eingeworfeneMünze = tastatur.nextDouble();
			if (eingeworfeneMünze > 2.0) {
				System.out.println("Nur Münzen bis 2€!");
			} else {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			}
		}
		AnzahlTickets -= anzahlKaufenderTickets;
		FahrscheinAusgabe();
    }
    
    public static void FahrscheinAusgabe() {
    	if (anzahlKaufenderTickets > 1) {
    		System.out.println("\nFahrscheine werden ausgegeben");
    	} else {
    		System.out.println("\nFahrschein wird ausgegeben");
    	}
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
        Rueckgabe();
    }
    
    public static void Rueckgabe() {
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f€\n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0)
            {
         	  System.out.println("");
         	 String str = "*************";
    		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  2 Euro %3.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
	          
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0)
            {
         	  System.out.println("");
         	  String str = "*************";
     		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  1 Euro %3.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5)
            {
         	  System.out.println("");
         	 String str = "*************";
    		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  50 cent %2.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
	 
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2)
            {
         	  System.out.println("");
         	 String str = "*************";
   		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  20 cent %2.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
	 
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1)
            {
         	  System.out.println("");
         	 String str = "*************";
   		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  10 cent %2.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
	 
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)
            {
         	  System.out.println("");
         	 String str = "*************";
   		  System.out.printf("%5.1s %.1s %.1s\n%3.1s %7.1s\n%.1s %11.1s\n%.1s  5 cent %3.1s\n%.1s %11.1s\n%3.1s %7.1s\n%5.1s %.1s %.1s", str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str);
	 
  	          rückgabebetrag -= 0.05;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
        fahrkartenbestellungErfassen();
    }
}