package src;

import java.util.Scanner;

public class Spaceship {
	
	private String[][] items = new String[100][4];
	private int repairkit_amount;
	private int token_amount;
	private int upgradekit_amount;
	private double max_health;
	public double health;
	private int inventory_items;
	public String current_torpedo;
	private String spaceship_name;
	private String[] inventory = new String[100];
	Scanner console = new Scanner(System.in).useDelimiter("\\n");;
	
	public String getName() {
		return this.spaceship_name;
	}
	
	public Spaceship(String string) {
		createShip(string);
	}

	protected void createShip (String name) {
		this.spaceship_name = name;
		this.max_health = 100;
		this.token_amount = 25;
		this.health = this.max_health;
		this.current_torpedo = "none";
		this.inventory[0] = "empty";
		declareItems();
	}
	
	
	protected void declareItems() {
		this.items[0][0] = "Laser Cannon";
		this.items[0][1] = "25";
		this.items[0][2] = "30";
		this.items[0][3] = "27";
		
		this.items[1][0] = "Photo Torpedo";
		this.items[1][1] = "70";
		this.items[1][2] = "50";
		this.items[1][3] = "57";
		
		this.items[2][0] = "Repair Kit";
		this.items[2][1] = "15";
		
		this.items[3][0] = "Upgrade Kit";
		this.items[3][1] = "50";
	}
	
	public void sendStatus() {
		int input;
		System.out.printf("\nSpacehip %s:\nHealth: %.2f HP\nLoaded Cannon: %s\nYour Tokens: %d\n!!! Commands: Type '0' to see spaceship control !!!\nAction: ", this.spaceship_name, this.health, this.current_torpedo, this.token_amount);
		input = console.nextInt();
		if (input == 0) {
			this.showCommands();
		} else if (input == 1) {
			this.showInventory();
		} else if (input == 2) {
			this.sendStatus();
		} else if (input == 3) {
			this.standBy();
		} else if (input == 4) {
			this.shop();
		} else if (input == 5) {
			Spaceship_controller.shootPlayer();
		} else if (input == 6) {
			this.loadWeapon();
		} else if (input == 7) {
			this.sendMessage();
		} else if (input == 8) {
			this.showChatHistory();
		} else if (input == 9) {
			this.repairShip();
		} else if (input == 10) {
			this.upgradeShip();
		} else if (input == 11) {
			Spaceship_controller.destroySpaceship();
		} else if (input == 42) {
			Spaceship_controller.destroyUniverse();
		} else if (input == 1337) {
			this.token_amount += 1337;
			System.out.printf("You got 1337 Tokens you jerk ;)\n");
			this.sendStatus();
		} else {
			this.sendStatus();
		}
		
	}
	
	protected void showCommands() {
		System.out.printf("Spaceship controls: \n0 : help\n1 : show inventory\n2 : see Spaceship status\n3 : standyBy\n4 : open shop to buy weapons\n5 : shoot Player\n6 : load Weapon\n7 : send message\n8 : show chat history\n9 : repair ship with repair kit\n10: upgrade ship with upgrade kit\n11: self desctruct spaceship\n\n");
		sendStatus();
	}
	
	
	protected void loadWeapon() {
		System.out.printf("Which Weapon do u want to load (your inventory items [loaded: 1, 2, 3, ...]) ?\ntype '0' to show Inventory items");
		System.out.printf("\nInput: ");
		int input = console.nextInt();
		if (input == 0) {
			this.showInventory();
		} else {
			this.current_torpedo = this.inventory[input];
			this.inventory[input] = null;
			sendStatus();
		}
		
	}
	
	
	public void getDamage(String Torpedo) {
		float damage = 0;
		if (Torpedo == this.items[0][0]) {
			damage = 30;
		} else if (Torpedo == this.items[1][0]) {
			damage = 50;
		} else {
			System.out.println("failed to shoot: unknown weapon");
		}
		this.health = this.health-damage;
		System.out.printf("Spaceship '%s' took %.2f damage from %s!\nHealth: %.2fHP\n\n", this.spaceship_name, damage, Torpedo, this.health);
	}
	
	protected void sendMessage() {
		boolean failure = true;
		int counter = 0;
		System.out.printf("Type your message here (use '.' as space):\n");
		String Message = console.next();
		Message = Message.replace(".", " ");
		System.out.printf("%s: %s\n\n", this.spaceship_name, Message);
		while (failure == true && counter != 999) {
			if (Spaceship_controller.chatHistory[counter] == null) {
				Spaceship_controller.chatHistory[counter] = this.spaceship_name + ": " + Message;
				failure = false;
			} else {
				counter++;
			}
		}
		sendStatus();
	}
	
	protected void standBy() {
		System.out.printf("shutting System to standyby....\n\n\n");
		Spaceship_controller.selectedPlayer = 404;
		Spaceship_controller.showPlayerList();
	}
	
	protected void shop() {
		System.out.printf("What item do u want to buy? \n0: %s [%s]\n1: %s [%s]\n2: %s [%s]\n3: Health %s [%s]\nInput: ", this.items[0][0], this.items[0][1], this.items[1][0], this.items[1][1], this.items[2][0], this.items[2][1], this.items[3][0], this.items[3][1]);
		int input = console.nextInt();
		if (input == 0) {
			if (token_amount > Integer.parseInt(this.items[0][1])-1) {
				token_amount -= Integer.parseInt(this.items[0][1]);
				this.addToInventory(this.items[0][0]);
				this.sendStatus();
			} else {
				System.out.printf("Not enough Tokens!\n");
				this.shop();
			}
		} else if (input == 1) {
			if (token_amount > Integer.parseInt(this.items[1][1])-1) {
				token_amount -= Integer.parseInt(this.items[1][1]);
				this.addToInventory(this.items[1][0]);
				this.sendStatus();
			} else {
				System.out.printf("Not enough Tokens!\n");
				this.shop();
			}
		} else if (input == 2) {
			if (token_amount > Integer.parseInt(this.items[2][1])-1) {
				token_amount -= Integer.parseInt(this.items[2][1]);
				System.out.printf("Repair Kit added to Inventory");
				this.repairkit_amount++;
				this.sendStatus();
			}
		} else if (input == 3) {
			if (token_amount > Integer.parseInt(this.items[3][1])-1) {
				token_amount -= Integer.parseInt(this.items[3][1]);
				System.out.printf("Upgrade Kit added to Inventory");
				this.upgradekit_amount++;
				this.sendStatus();
			} else {
				this.shop();
			}
		} else if (input == 4) {
			this.sendStatus();
		} else {
			this.shop();
		}
	}
	
	public void getTokens(String input) {
		int award = 0;
		if (input == this.items[0][0]) {
			award = Integer.parseInt(this.items[0][3]);
			token_amount += award;
			System.out.printf("You got %d Tokens!\n", award);
		} else if (input == this.items[1][0]) {
			award = Integer.parseInt(this.items[1][3]);
			token_amount += award;
			System.out.printf("You got %d Tokens!\n", award);
		}
	}
	
	public void addToInventory(String item) {
		boolean failure = true;
		int counter = 1;
		while (failure == true && counter != 99) {
			if (this.inventory[0] == "empty") {
				this.inventory[0] = "Loaded:";
				this.inventory[1] = item;
				this.inventory_items = 1;
				System.out.printf("Item added to Inventory: %s\n\n", this.inventory[1]);
				failure = false;
				sendStatus();
				break;
			}
			if (this.inventory[counter] == null) {
				this.inventory[counter] = item;
				this.inventory_items = counter;
				System.out.printf("Item added to Inventory: %s\n\n", this.inventory[counter]);
				failure = false;
				sendStatus();
				break;
			} else {
				counter++;
			}
		}
	}
	
	public void showChatHistory() {
		int counter = 0;
		while (Spaceship_controller.chatHistory[counter] != null && counter != 999) {
			System.out.println(Spaceship_controller.chatHistory[counter]);
			counter++;
		}
	}

	public void showInventory() {
		System.out.printf("Items in Inventory: %d\n", this.inventory_items);
		System.out.printf(java.util.Arrays.toString(this.inventory));
		System.out.printf("\nRepair Kits: %d", this.repairkit_amount);
		System.out.printf("\nUpgrade Kits: %d", this.upgradekit_amount);
		System.out.printf("\n\n");
		sendStatus();
	}
	
	
	protected void repairShip() {
		if (this.repairkit_amount == 0) {
			System.out.printf("\nU dont own any repait kit...\n");
			sendStatus();
		} else {
			System.out.printf("How many Repair Kits do u want to use?\nAviable Repair Kits: %d\nInput: ", this.repairkit_amount);
			int input = console.nextInt();
			if (input > this.repairkit_amount) {
				System.out.println("U dont have enough repair kits!");
				repairShip();
			} else {
				int recoveredhealth = 25*input;
				this.health = health+recoveredhealth;
				if (this.health > max_health) {
					this.health = max_health;
				}
				System.out.println("Repairing spaceship...");
				sendStatus();
			}
		}
	}
	
	protected void upgradeShip() {
		System.out.printf("How many Upgrade Kits do u want to use?\nAviable Uograde Kits: %d\nInput: ", this.upgradekit_amount);
		int input = console.nextInt();
		int addedHealth = 25*input;
		this.max_health = max_health+addedHealth;
		this.health = max_health;
		System.out.println("Upgrading spaceship...");
		sendStatus();
	}
	
	public void selfDestruction() {
		System.exit(0);
	}
	

}
