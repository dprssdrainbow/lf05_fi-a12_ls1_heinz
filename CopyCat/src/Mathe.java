import java.util.Scanner;
public class Mathe {

	public static void main(String[] args) {
		
		System.out.printf("Willkommen zu meinem Programm !\n- Mathe -\nEntiwckelt von Sophia Heinz/oszimt/FI-A12");
		AufgabenAuswahl();	
		
	}
	
	public static void AufgabenAuswahl() {
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("\n\nWählen Sie eine Aufgabe: 5 / 6 / 7\n0 um das Programm zu beenden.\nEingabe: ");
		int i = tastatur.nextInt();
		
		switch (i) {
		case 0:
			System.out.println("\nDanke fürs Verwenden meines Programms !");
			return;
		case 5:
			Aufgabe5();
			break;
		case 6:
			Aufgabe6();
			break;
		case 7:
			Aufgabe7();
			break;
		default:
				System.out.println("Falsche Eingabe !\n");
				AufgabenAuswahl();
				break;
		}
	}
	
	public static double quadrat(double x) {
		x = x*x;
		return x;
	}
	
	public static double hypotenuse(double kathete1, double kathete2) {
		
		double hypotenuse = Math.sqrt(quadrat(kathete1)+quadrat(kathete2));
		return hypotenuse;
	}
	
	public static void Aufgabe5() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Quadratberechnung: ");
		double x = tastatur.nextDouble();
		x = quadrat(x);
		System.out.println("Das Ergebnis ist: " + x);
	}
	
	public static void Aufgabe6() {
		
	}

	public static void Aufgabe7() {
		Scanner tastatur = new Scanner(System.in);
	
		System.out.println("Berechnung der Hypotenuse: ");
		System.out.printf("Kathete 1: ");
		double kathete1 = tastatur.nextDouble();
		System.out.printf("Kathete 2: ");
		double kathete2 = tastatur.nextDouble();
		double h = hypotenuse(kathete1, kathete2);
		System.out.println("Das Ergebnis ist: " + h);
	}
}
